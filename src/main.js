// import Vue from 'vue'
// import App from './App.vue'
//
// Vue.config.productionTip = false
//
// new Vue({
//   render: h => h(App),
// }).$mount('#app')

import { createApp } from 'vue'
import App from './App.vue'
import './style.css'
import router from './router'
import store from './store'

// Vue.use(VueRouter)
//
// new Vue({
//     render: h => h(App),
//     el: '#app',
//     router
// }).mount(App)

const app = createApp(App)
app.use(router)
app.use(store)
app.mount('#app')



// const app = Vue.createApp({
//
// })
// createApp(VueRouter)
// createApp(App).use(router).mount('#app')


