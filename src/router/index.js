import { createRouter, createWebHistory } from 'vue-router'
import Auth from "@/pages/Auth";
import MainPage from "@/pages/MainPage";

const routes = [
    {
        path: '/',
        name: 'Authorization',
        component: Auth
    },
    {
        path: '/mainPage',
        name: 'MainPage',
        component: MainPage
    }

]

const router = createRouter({ history: createWebHistory(), routes})
export default router
// export default new VueRouter({
//     mode: 'history',
//     routes: [
//         {
//             path: '/',
//             name: 'Authorization',
//             component: Auth
//         }
//     ]
// })