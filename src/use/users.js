import {ref} from 'vue'
import {userFetch} from "@/use/fetch";

export async function useUsers(){
    const loaded = ref(false)
    const {response: users, request} = userFetch('https://jsonplaceholder.typicode.com/users?_limit=1')

    if (!loaded.value){
        await request()
        loaded.value = true
    }

    return {users}
}